# Running full on-prem TeamCity locally
The example docker-compose stack seems to just work: https://github.com/JetBrains/teamcity-docker-samples/blob/master/compose-ubuntu/docker-compose.yml

# Running TeamCity cloud with self-hosted build agents on Kubernetes
- create Kubernetes cluster
- create namespace
    ```bash
    kubectl create ns teamcity
    ```
- create a role with service account bound to it ([thanks
  blog](https://devopscube.com/create-kubernetes-role/)). Yep, this role is way
  too permissive but I tried with fewer permissions as shown in [the official
  doco](https://www.jetbrains.com/help/teamcity/setting-up-teamcity-for-kubernetes.html#Requirements)
  and it didn't work
  > Error from server (Forbidden): pods is forbidden: User
  > "system:serviceaccount:teamcity:default" cannot list resource "pods" in API
  > group "" in the namespace "teamcity"
  
  ...anyway, back on-topic, here's the command:
    ```bash
    kubectl apply -f service-account.yaml
    ```
- describe the service account to get the secret
    ```bash
    $ kubectl describe sa -n teamcity the-god
    ...
    Tokens:              the-god-token-jrwsj
    ...
    ```
- describe that secret to get the bearer token ([thanks
  SO](https://stackoverflow.com/a/53182585/1410035))
    ```bash
    $ kubectl describe secret -n teamcity the-god-token-jrwsj
    ...
    token:      eyJh...
    ...
    ```
- open the TeamCity web UI
- open the *Settings* for the "Root Project"
- open the *Cloud Profiles* menu option
- create a new profile
- set the *Kubernetes API server URL* to whatever it needs to be, probably
  something like
    ```
    https://<some IP>/
    https://<some hostname>/
    ```
- set *Kubernetes namespace* = `teamcity`
- set *Authentication strategy* = `Token`
- set *Token* to the bearer token you grabbed earlier. It's a long string that
  starts with `ey...`
- press *Test connection* and it should be successful
- Click *Add image* under *Agent images*
  - *Pod specification* = `Use custom pod template`
  - *Pod template content* should be the contents of `./pod-template.yaml`
  - *Agent pool* = `<Project pool>` (or whatever, probably doesn't matter)
  - if clicking *Add* doesn't work, show advanced options to set the *Agent name
    prefix*, which is required but not shown :facepalm:
- *Save* the cloud profile
- the system should automatically start a build agent pod for you. You can see
  this if you list the pods in Kubernetes or in the TeamCity UI by going to
  Agent (top of screen) -> (select the pool) -> (select the cloud profile) and
  you'll see a *Running Instances* list that should have one entry.
- create a job
  - add a build step
  - set the type to *Command Line*
  - insert some bash, like
    ```bash
    set -x
    date
    pwd
    id
    node --version
    ```
  - set the docker image to `node:14.17`
  - save the job
- trigger a run. You should see the Kubernetes cluster create a build agent, if
  it's not already running and the job will run.
